#include <stdio.h>
#include <stdlib.h>
#include "numero_por_extenso.h"

int main(){
	int numero;
	
	printf("Digite um numero entre 1 e 10000\n");
	scanf("%d", &numero);
	
	numero_por_extenso(numero);
	
	return 0;
}
