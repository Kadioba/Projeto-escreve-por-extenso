int main() {
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "numero_por_esntenso.h"

void numero_por_extenso(int n) {
	
	char unidades[20][20] = {"zero", "um", "dois", "três", "quatro", "cinco", "seis", "sete", "oito", "nove", "dez", "onze", "doze", "treze", "quatorze", "quinze", "dezesseis", "dezessete", "dezoito", "dezenove"};
	char dezenas[10][20] = {"", "", "vinte", "trinta", "quarenta", "cinquenta", "sessenta", "setenta", "oitenta", "noventa"};
	char centenas[10][20] = {"", "cem", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos"};
	char milhares[10][20] = {"", "mil", "dois mil", "três mil", "quatro mil", "cinco mil", "seis mil", "sete mil", "oito mil", "nove mil"};

	int unidade = n % 10;
    int dezena = (n / 10) % 10;
    int centena = (n / 100) % 10;
    int milhar = (n / 1000) % 10;
    
    char extenso[100] = "";
    
	if (n == 0) {
		strcpy(extenso, "zero");
	}

    else {

        if (milhar > 0) {
            strcat(extenso, milhares[milhar]);
            if (centena > 0 || dezena > 0 || unidade > 0) {
                strcat(extenso, " e ");
            }
        }

        if (centena > 0) {
            if (centena == 1 && dezena == 0 && unidade == 0) {
                strcat(extenso, "cem");
            }
            else {
                strcat(extenso, centenas[centena]);
                if (dezena > 0 || unidade > 0) {
                    strcat(extenso, " e ");
                }
            }
        }
        
        if (dezena > 0) {
            if (dezena == 1) {
                if (unidade == 0) {
                    strcat(extenso, "dez");
                }
                else if (unidade == 1) {
                    strcat(extenso, "onze");
                }
                else if (unidade == 2) {
                    strcat(extenso, "doze");
                }
                else if (unidade == 3) {
                    strcat(extenso, "treze");
                }
                else if (unidade == 4) {
                    strcat(extenso, "quatorze");
                }
                else if (unidade == 5) {
                    strcat(extenso, "quinze");
                }
                else if (unidade == 6){
					strcat(extenso, "dezesseis");
				}
				else if (unidade == 7) {
                strcat(extenso, "dezessete");
				}
				else if (unidade == 8) {
					strcat(extenso, "dezoito");
				}
				else if (unidade == 9) {
					strcat(extenso, "dezenove");
				}
			}
			else {
				strcat(extenso, dezenas[dezena]);
				if (unidade > 0) {
					strcat(extenso, " e ");
				}
			}
		}

		if (unidade > 0 && dezena != 1) {
			strcat(extenso, unidades[unidade]);
		}
    
	}
    printf("%s", extenso);
}
return 0;
}
